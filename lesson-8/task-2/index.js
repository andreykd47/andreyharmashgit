/**
 * Доработать форму из 1-го задания.
 * 
 * Добавить обработчик сабмита формы.
 * 
 * Для того что бы увидеть результат откройте index.html файл в браузере.
 * 
 * Обязательно!
 * 1. При сабмите формы страница не должна перезагружаться
 * 2. Генерировать ошибку если пользователь пытается сабмитить форму с пустыми или содержащими только пробел(ы) полями.
 * 3. Если поля формы заполнены и пользователь нажимает кнопку Вход → вывести в консоль объект следующего вида
 * {
 *  email: 'эмейл который ввёл пользователь',
 *  password: 'пароль который ввёл пользователь',
 *  remember: 'true/false'
 * }
*/

// РЕШЕНИЕ

const labelEmail = document.createElement('label');
labelEmail.setAttribute('for', 'email');
labelEmail.innerHTML = 'Электропочта';

const inputEmail = document.createElement('input');
inputEmail.setAttribute('type', 'email');
inputEmail.setAttribute('class', 'form-control');
inputEmail.setAttribute('id', 'email');
inputEmail.setAttribute('placeholder', 'Введите свою электропочту');

const firstFormGroup = document.createElement('div');
firstFormGroup.setAttribute('class', 'form-group');
firstFormGroup.prepend(labelEmail);
firstFormGroup.append(inputEmail);

// Password form group

const labelPass = document.createElement('label');
labelPass.setAttribute('for', 'password');
labelPass.innerHTML = 'Пароль';

const inputPass = document.createElement('input');
inputPass.setAttribute('type', 'password');
inputPass.setAttribute('class', 'form-control');
inputPass.setAttribute('id', 'password');
inputPass.setAttribute('placeholder', 'Введите пароль');

const secondFormGroup = document.createElement('div');
secondFormGroup.setAttribute('class', 'form-group');
secondFormGroup.prepend(labelPass);
secondFormGroup.append(inputPass);

// Checkbox form group

const labelCheck = document.createElement('label');
labelCheck.setAttribute('for', 'exampleCheck1');
labelCheck.setAttribute('class', 'form-check-label');
labelCheck.innerHTML = 'Запомнить меня';

const inputCheck = document.createElement('input');
inputCheck.setAttribute('type', 'checkbox');
inputCheck.setAttribute('class', 'form-check-input');
inputCheck.setAttribute('id', 'exampleCheck1');

const lastFormGroup = document.createElement('div');
lastFormGroup.setAttribute('class', 'form-group form-check');
lastFormGroup.prepend(inputCheck);
lastFormGroup.append(labelCheck);

// Button

const submit = document.createElement('button');
submit.setAttribute('type', 'submit');
submit.setAttribute('class', 'btn btn-primary');
submit.innerHTML = 'Вход';

// Form wrap settings

const form = document.getElementById('form');
form.prepend(firstFormGroup);
form.append(secondFormGroup);
form.append(lastFormGroup);
form.append(submit);


// Validating

const formSubmit = document.getElementById('form');

formSubmit.addEventListener('submit', (sub) => {

    sub.preventDefault();

    const valueEmail = inputEmail.value.trim();

    const valuePass = inputPass.value.trim();

    if(valueEmail == null || valueEmail == "" || valuePass == null || valuePass == "") {
        
        throw new Error('All Field is Required!!!');

    }

    const stateCheck = inputCheck.checked ? "true" : "false";

    const logData = {
        email: valueEmail,
        password: valuePass,
        remember: stateCheck,
    };

    console.log(logData);

});  