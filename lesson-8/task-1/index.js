/**
 * Создать форму динамически при помощи JavaScript.
 * 
 * В html находится пример формы которая должна быть сгенерирована.
 * 
 * Для того что бы увидеть результат откройте index.html файл в браузере.
 * 
 * Обязательно!
 * 1. Для генерации элементов обязательно использовать метод document.createElement
 * 2. Для установки атрибутов элементам обязательно необходимо использовать document.setAttribute
 * 3. Всем созданным элементам необходимо добавить классы как в разметке
 * 4. После того как динамическая разметка будет готова необходимо удалить код в HTML который находится между комментариями
*/

// РЕШЕНИЕ


// Email form group

const labelEmail = document.createElement('label');
labelEmail.setAttribute('for', 'email');
labelEmail.innerHTML = 'Электропочта';

const inputEmail = document.createElement('input');
inputEmail.setAttribute('type', 'email');
inputEmail.setAttribute('class', 'form-control');
inputEmail.setAttribute('id', 'email');
inputEmail.setAttribute('placeholder', 'Введите свою электропочту');

const firstFormGroup = document.createElement('div');
firstFormGroup.setAttribute('class', 'form-group');
firstFormGroup.prepend(labelEmail);
firstFormGroup.append(inputEmail);

// Password form group

const labelPass = document.createElement('label');
labelPass.setAttribute('for', 'password');
labelPass.innerHTML = 'Пароль';

const inputPass = document.createElement('input');
inputPass.setAttribute('type', 'password');
inputPass.setAttribute('class', 'form-control');
inputPass.setAttribute('id', 'password');
inputPass.setAttribute('placeholder', 'Введите пароль');

const secondFormGroup = document.createElement('div');
secondFormGroup.setAttribute('class', 'form-group');
secondFormGroup.prepend(labelPass);
secondFormGroup.append(inputPass);

// Checkbox form group

const labelCheck = document.createElement('label');
labelCheck.setAttribute('for', 'exampleCheck1');
labelCheck.setAttribute('class', 'form-check-label');
labelCheck.innerHTML = 'Запомнить меня';

const inputCheck = document.createElement('input');
inputCheck.setAttribute('type', 'checkbox');
inputCheck.setAttribute('class', 'form-check-input');
inputCheck.setAttribute('id', 'exampleCheck1');

const lastFormGroup = document.createElement('div');
lastFormGroup.setAttribute('class', 'form-group form-check');
lastFormGroup.prepend(inputCheck);
lastFormGroup.append(labelCheck);

// Button

const submit = document.createElement('button');
submit.setAttribute('type', 'submit');
submit.setAttribute('class', 'btn btn-primary');
submit.innerHTML = 'Вход';

// Form wrap settings

const form = document.getElementById('form');
form.prepend(firstFormGroup);
form.append(secondFormGroup);
form.append(lastFormGroup);
form.append(submit);

