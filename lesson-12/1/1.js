/**
 * Примерные данные для заполнения, вы можете использовать свои данные.
 *  Имя: "Firefox",   Компания: "Mozilla",        Процент: "8.01%"        
 *  Имя: "Chrome",    Компания: "Google",         Процент: "68.26%"
 *  Имя: "Edge",      Компания: "Microsoft",      Процент: "6.67%"        
 *  Имя: "Opera",     Компания: "Opera Software", Процент: "1.31%"
 * 
 */

/* Решение */

const allCompanyInfo = [];
const browserInput = document.getElementById('browser');
const companyInput = document.getElementById('company');
const percentInput = document.getElementById('percent');
const submitInput = document.getElementById('submit');
const viewData = document.getElementById('viewData');
const viewResult = document.getElementById('result');

submitInput.setAttribute('disabled', 'disabled');


/* Validation */

function checkValid(mainInput, val) {

    const trimVal = val.trim();

    if( trimVal.length <= 2 ) {
        submitInput.setAttribute('disabled', 'disabled');
        mainInput.classList.add('error');
    } else if ( trimVal.length > 2  ) {
        mainInput.classList.remove('error');
    } 

}

function checkPercent(mainInput, val) {

    String.prototype.isNumber = function(){
        return /^[0-9]*\.?[0-9]*$/.test(this);   // Для дробных чисел добавил точку
    }

    if (val.isNumber() === true && parseFloat(val) <= 100 && parseFloat(val) > 0 ) {
        mainInput.classList.remove('error');
        submitInput.removeAttribute('disabled');

        return finalPercentSubmit = true;

    } else {
        mainInput.classList.add('error');
        submitInput.setAttribute('disabled', 'disabled');
    } 

}

function activeSubmit(percentSyb) {
    if ( browserInput.value.length > 2 && companyInput.value.length > 2 && percentSyb ) {
        submitInput.removeAttribute('disabled');
    } 
}

browserInput.addEventListener('input', function() {
    const inputCheck = this;
    const inputVal = this.value;
    checkValid(inputCheck, inputVal);
    activeSubmit();
});
companyInput.addEventListener('input', function() {
    const inputCheck = this;
    const inputVal = this.value;
    checkValid(inputCheck, inputVal);
    activeSubmit();
});
percentInput.addEventListener('input', function() {
    const inputCheck = this;
    const inputVal = this.value;
    let finalPercentSubmit = false;
    checkPercent(inputCheck, inputVal);
    activeSubmit(finalPercentSubmit);
});


// Save data

const mainForm = document.getElementById('form');

mainForm.addEventListener('submit', (event) => {

    event.preventDefault();

    const singleBrowser = {};
    const formData = new FormData(event.target);

    const browserVal = formData.get('browser');
    const companyVal = formData.get('company');
    const percentVal = formData.get('percent');
 

    singleBrowser['name'] = browserVal;
    singleBrowser['company'] = companyVal;
    singleBrowser['marketShare'] = percentVal;   
    
    allCompanyInfo.push(singleBrowser);
    mainForm.reset();

 });


 // Display best browser data

 viewData.addEventListener('click',function(){
    
    if (allCompanyInfo.length === 0) {

        viewResult.innerHTML = `<b style="color: red;">"Недостаточно данных"</b>`;

    } else if (allCompanyInfo.length === 1) {

        const { name, company, marketShare } = allCompanyInfo[0];
        viewResult.innerHTML = `<b style="color: green;">"Самый востребованный браузер это ${name} от компании ${company} с процентом использования ${marketShare}%"</b>`;
    
    } else if (allCompanyInfo.length >= 2) {

        const maxParam = allCompanyInfo.reduce(function(prev, current) {
            
            if (+current.marketShare > +prev.marketShare) {
                return current;
            } else {
                return prev;
            }
            
        });

        const { name, company, marketShare } = maxParam;
        viewResult.innerHTML = `<b style="color: green;">"Самый востребованный браузер это ${name} от компании ${company} с процентом использования ${marketShare}%"</b>`;
    
    } else {

        viewResult.innerHTML = `<b style="color: red;">"Ошибка обработки данных"</b>`;

    }

});
