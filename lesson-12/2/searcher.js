function searcher(brorswersObjects) {

    const maxParam = brorswersObjects.reduce(function(prev, current) {
            
        if (+current.marketShare > +prev.marketShare) {
            return current;
        } else {
            return prev;
        }

    });

    const { name, company, marketShare } = maxParam;
    const viewResult = `"Самый востребованный браузер это ${name} от компании ${company} с процентом использования ${marketShare}%"`;

    console.log(viewResult);
}

export { searcher };