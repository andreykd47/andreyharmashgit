const browsers = [
    {
        name: "Firefox",
        company: "Mozilla",
        marketShare: "8.01"
    },
    {
        name: "Chrome",
        company: "Google",
        marketShare: "68.26"
    },
    {
        name: "Edge",
        company: "Microsoft",
        marketShare: "6.67"
    },
    {
        name: "Opera",
        company: "Opera Software",
        marketShare: "1.31"
    }
]

export { browsers };