/**
 * Задача 5.
 * 
 * Написать скрипт, который возвращает массив с объектами - характеристиками элементов массива.
 * Объекты должен иметь такие свойства: 
 *      value - значение элемента;
 *      type - тип данных элемента. 
 * 
 * Если тип данных элемента СТРОКА объекты должны иметь дополнительное свойство length.
 * Если тип данных элемента не ЧИСЛО и не СТРОКА объекты должны иметь дополнительное 
 * свойство error с текстом 'Invalid type'.
 *
 * Пример: arr -> ['Доброе утро!', 2, {}]
 *         objArr = [
 *              { value: 'Доброе утро!', type: 'string', length: 12 },
 *              { value: 2, type: 'number' },
 *              { value: {}, type: 'object', error: 'Invalid type' }
 *         ];
 * 
 * Условия:
 *  - Обязательно использовать встроенный метод массива map.
 */

const array = ['Доброе утро!', null, 2, 'Привет', NaN, () => {},
    [], 'Добрый вечер!', {}, 'ананас', '#', 'До свидания!'
]; // ИЗМЕНЯТЬ ЗАПРЕЩЕНО


// РЕШЕНИЕ

// const newArr = array.map(function(item) {

//     if(typeof item == 'string') {

//         return { value: item, type: typeof item, length: item.length };

//     } else if (typeof item == 'number') {

//         return { value: item, type: typeof item };

//     } else {

//         return { value: item, type: typeof item, error: 'Invalid type' }

//     }

// });


const newArr = array.map(function(item) {     // <-- новое решение

    const obj = {
        value: item,
        type: typeof item,
    } 

    if(typeof item === 'string') {
        obj.length = item.length;
    }

    if(typeof item !== 'string' && typeof item !== 'number') {
        obj.error = 'Invalid type';
     } 


     return obj;

});

console.log(newArr);
