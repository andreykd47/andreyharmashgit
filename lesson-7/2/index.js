/**
 * Задача 2.
 *
 * Напишите скрипт, который проверяет идентичны ли массивы.
 * Если массивы полностью идентичны - в переменную flag присвоить значение true, 
 * иначе - false. 
 * 
 * Пример 1: const arr1 = [1, 2, 3];
 *           const arr2 = [1, '2', 3];
 *           flag -> false
 *
 * Пример 2: const arr1 = [1, 2, 3];
 *           const arr2 = [1, 2, 3];
 *           flag -> true
 *
 * Пример 3: const arr1 = [];
 *           const arr2 = arr1;
 *           flag -> true
 *
 * Пример 4: const arr1 = [];
 *           const arr2 = [];
 *           flag -> true
 * 
 * Условия:
 * - Обязательно проверять являются ли сравниваемые структуры массивами;
 *
*/

const arr1 = [1, 2, 3];
const arr2 = [1, 2, 3];

let flag = '';

// РЕШЕНИЕ

function compare(a, b) {

    if(a.length != b.length) {
        return false;
    }
       
    for(let i = 0; i < a.length; i++) {
        if(a[i] != b[i]) {
            return false;
        }
    }
       
    return true; 

}


if (Array.isArray(arr1) && Array.isArray(arr2)) {  // <-- Эта проверка ведь подходит для всех возможных примеров, где сдесь ошибка?

    console.log(compare(arr1, arr2));

} else {

    throw new Error('Вы сравниваете не масивы!');

}