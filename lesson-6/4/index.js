/**
 * Задача 4.
 *
 * Создайте функцию `getDivisors`, которая принимает число в качестве аргумента.
 * Функция возвращает массив его делителей (чисел, на которое делится данное число начиная от 1 и заканчивая самим собой).
 *
 * Условия:
 * - Генерировать ошибку, если в качестве входного аргумента был передан не числовой тип;
 * - Генерировать ошибку, если в качестве входного аргумента был передано число меньшее, чем 1.
 * 
 * Заметки:
 * - В решении вам понадобится использовать цикл с перебором массива.
 */

// РЕШЕНИЕ

const arr = [];

function getDivisors(num) {

    if (isNaN(num)) {

        throw new Error('Веше число это не число, введите число!');

    } else {

        if (Number(num) < 1) {

            throw new Error('Веше число меньше 1. Веедите число более большое!');

        } else {

            for (let i = 1; i <= num; i++) {
                if (num % i == 0) {
                    arr.push([i]);
                }
            }

            return arr;

        }

    } 

}

console.log(getDivisors(12)); 

// У меня получилось вродебы правильно, но меня смущает финальный вид масива. Он какой-то не такой. Я не понял как его преобразовать в обычный масив.

