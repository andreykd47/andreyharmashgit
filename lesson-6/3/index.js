/**
 * Задача 3.
 *
 * Создайте функцию `isEven`, которая принимает число качестве аргумента.
 * Функция возвращает булевое значение.
 * Если число, переданное в аргументе чётное — возвращается true.
 * Если число, переданное в аргументе нечётное — возвращается false.
 *
 * Условия:
 * - Генерировать ошибку, если в качестве входного аргумента был передан не числовой тип;
 */

// РЕШЕНИЕ


// function isEven(val) {
//     if (isNaN(val)) {
//         throw new Error('Веше число это не число, введите число!');
//     } else if (!(val % 2) === true) {
//         return true;
//     } else {
//         return false;
//     }
// }

function isEven(val) {
    if (isNaN(val)) {
        throw new Error('Веше число это не число, введите число!');
    } else {
        return (val % 2) === 0;
    }
}

// Тут ровно и так возвращает true/false - я не понимаю, как можно еще упростить это выражение, надеюсь исправил правильно


console.log(isEven2(3)); 
console.log(isEven2(4)); 
