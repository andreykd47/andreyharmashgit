/**
 * Доработайте функцию что бы она возвращала объект из переданного вложенного массива
 * 
 * Фукнция принимает 1 аргумента
 * 1. Массив из массивов который содержит 2 элемента — [ [ element1, element2 ] ]
 * 
 * ЗАПРЕЩЕНО ИСПОЛЬЗОВАТЬ ВСТРОЕННЫЙ МЕТОД Object.fromEntries
 * 
 * Обратите внимание!
 * 1. Генерировать ошибку если второй элемент вложенного массива не число, не строка или не null
 * 2. Обязательно использовать деструктуризацию при извлечении элементов массива
 * 3. Если в качестве второго аргумента был передан массив вида [ [ element1, element2 ] ], то его так же нужно преобразовать в объект
 * 4. Для перебора массива можно воспользоваться циклом for..of.
*/

// const fromEntries = (entries) => {
//     const obj = {};

//     return obj;
// };


const fromEntries = (entries) => {

    return entries.reduce((acc, entry) => {

        let value = entry[1];

        let type = typeof value;

        if(type === 'number' || type === 'string' || type === null || type === 'object') { // я добавил еще обьект ибо в примере мы его тоже принимаем

            if(typeof value === 'object') {

                value = fromEntries(value);
    
            } 
    
            return {...acc, [entry[0]]: value};
            
        } else {

            throw new Error('Вы передаете не те значения!!!');

        }

    }, {});

};


console.log(fromEntries([['name', 'John'], ['age', 35]])); // { name: 'John', age: 35 }
console.log(fromEntries([['name', 'John'], ['address', [['city', 'New  York']]]])); // { name: 'John', address: { city: 'New  York' } }

