/**
 * Задание 5
 * 
 * Используя цикл найти факториал числа.
 * Факториал числа вывести в консоль.
 */

const number = prompt('Введите число:'); // ИЗМЕНЯТЬ ЗАПРЕЩЕНО

//РЕШЕНИЕ

let factorialResult = 1;

for (factorialItem = 0; factorialItem < number; factorialItem++) {
    
    factorialResult = factorialResult * (number - factorialItem);

}

console.log('Факториал от', number, '=', factorialResult);